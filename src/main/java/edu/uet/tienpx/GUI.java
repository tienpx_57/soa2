package edu.uet.tienpx;

import org.w3c.dom.Document;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.Spring;
import javax.swing.SpringLayout;
import javax.swing.border.Border;

/**
 * Created by John on 11/19/2015.
 */
public class GUI {
    private JFrame frame;

    private JTabbedPane tabPane;

    private JPanel encryptPanel;
    private JPanel encryptLeftPanel;
    private JPanel encryptRightPanel;


    private JLabel publickey;
    private JButton selectPublicKey;
    private JLabel rawXMLFile;
    private JButton selectRawXMLFile;
    private JButton encrypt;
    private JButton saveEncrypted;
    private JTextArea previewRaw;
    private JTextField xpathExpress;

    private JPanel decryptPanel;
    private JPanel decryptLeftPanel;
    private JPanel decryptRightPanel;
    private JLabel privatekey;
    private JButton selectPrivateKey;
    private JLabel encryptedXMLFile;
    private JButton selectEncryptedXMLFile;
    private JButton decrypt;
    private JButton saveDecrypted;
    private JTextArea previewEncrypt;
    private SpringLayout decryptLayout;
    private GridLayout encryptLayout;

    private JFileChooser chooser;
    private App app;

    private Document encryptedDoc;
    private Document rawDoc;

    private void createFrameAndTabPanel(){
        frame = new JFrame("XML Crypter");
        frame.setSize(1200, 600);

        tabPane = new JTabbedPane();
        frame.add(tabPane);
    }
    private void createEncryptPanel(){
        encryptPanel = new JPanel();
        encryptPanel.setLayout(new BorderLayout());
        tabPane.add("Encrypt", encryptPanel);
        encryptLeftPanel = new JPanel();
        encryptLayout = new GridLayout(3,2);
        encryptLeftPanel.setLayout(encryptLayout);
        encryptLeftPanel.setPreferredSize(new Dimension(600, 600));
        encryptPanel.add(encryptLeftPanel, BorderLayout.WEST);
        encryptRightPanel = new JPanel();
        encryptRightPanel.setPreferredSize(new Dimension(580, 600));
        encryptPanel.add(encryptRightPanel, BorderLayout.EAST);
    }

    private void createDecryptPanel(){
        decryptPanel = new JPanel();
        decryptPanel.setLayout(new BorderLayout());
        tabPane.add("Decrypt", decryptPanel);
        decryptLeftPanel = new JPanel();
        //decryptLayout = new SpringLayout();
        decryptLeftPanel.setLayout(new GridLayout(3, 2));
        decryptLeftPanel.setPreferredSize(new Dimension(600, 600));
        decryptPanel.add(decryptLeftPanel, BorderLayout.WEST);
        decryptRightPanel = new JPanel();
        decryptRightPanel.setPreferredSize(new Dimension(580, 600));
        decryptPanel.add(decryptRightPanel, BorderLayout.EAST);
    }

    private void createContainerStructure(){
        createFrameAndTabPanel();
        createEncryptPanel();
        createDecryptPanel();
    }

    private void createPreview(){
        previewRaw = new JTextArea("");
        previewRaw.setLineWrap(true);
        previewRaw.setWrapStyleWord(true);
        previewRaw.setPreferredSize(new Dimension(560, 600));
        Border border = BorderFactory.createLineBorder(Color.BLACK);
        previewRaw.setBorder(BorderFactory.createCompoundBorder(border,
                BorderFactory.createEmptyBorder(10, 10, 10, 10)));
        encryptRightPanel.add(previewRaw);

        previewEncrypt = new JTextArea("");
        previewEncrypt.setLineWrap(true);
        previewEncrypt.setWrapStyleWord(true);
        previewEncrypt.setPreferredSize(new Dimension(560, 600));
        previewEncrypt.setBorder(BorderFactory.createCompoundBorder(border,
                BorderFactory.createEmptyBorder(10, 10, 10, 10)));
        decryptRightPanel.add(previewEncrypt);
    }

    public GUI(App handle){
        app = handle;
        createContainerStructure();
        createPreview();

        chooser = new JFileChooser();
        createButton();

        frame.setLocationRelativeTo(null);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }

    private void createButton(){


        publickey = new JLabel("Path to public key file");
        publickey.setAlignmentX(Component.CENTER_ALIGNMENT);
        encryptLeftPanel.add(publickey);

        selectPublicKey = new JButton("Choose Public Key");
        selectPublicKey.setAlignmentX(Component.CENTER_ALIGNMENT);
        encryptLeftPanel.add(selectPublicKey);

        privatekey = new JLabel("Path to private key file");
        privatekey.setAlignmentX(Component.CENTER_ALIGNMENT);
        decryptLeftPanel.add(privatekey);

        selectPrivateKey = new JButton("Choose Private Key");
        selectPrivateKey.setAlignmentX(Component.CENTER_ALIGNMENT);
        decryptLeftPanel.add(selectPrivateKey);

        rawXMLFile = new JLabel("Path to raw xml file");
        rawXMLFile.setAlignmentX(Component.CENTER_ALIGNMENT);
        encryptLeftPanel.add(rawXMLFile);

        selectRawXMLFile = new JButton("Choose Raw XML file");
        selectRawXMLFile.setAlignmentX(Component.CENTER_ALIGNMENT);
        encryptLeftPanel.add(selectRawXMLFile);

        encryptedXMLFile = new JLabel("Path to encrypted XML file");
        decryptLeftPanel.add(encryptedXMLFile);
        selectEncryptedXMLFile = new JButton("Choose encrypted XML file");
        decryptLeftPanel.add(selectEncryptedXMLFile);


        xpathExpress = new JTextField();
        xpathExpress.setPreferredSize(new Dimension(100, 14));
        xpathExpress.setAlignmentX(Component.CENTER_ALIGNMENT);
        encryptLeftPanel.add(xpathExpress);

        decrypt = new JButton("Decrypt");
        decryptLeftPanel.add(decrypt);

        encrypt = new JButton("Encrypt");
        encryptLeftPanel.add(encrypt);
        //SpringUtilities.makeCompactGrid(encryptLeftPanel, 2, 2, 6, 6, 100, 10);

        selectPrivateKey.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int ret = chooser.showOpenDialog(frame);
                if (ret == JFileChooser.APPROVE_OPTION) {
                    privatekey.setText(chooser.getSelectedFile().getAbsolutePath());
                }
            }
        });

        selectPublicKey.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int ret = chooser.showOpenDialog(frame);
                if (ret == JFileChooser.APPROVE_OPTION) {
                    publickey.setText(chooser.getSelectedFile().getAbsolutePath());
                }
            }
        });

        selectRawXMLFile.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int ret = chooser.showOpenDialog(frame);
                if (ret == JFileChooser.APPROVE_OPTION) {
                    rawXMLFile.setText(chooser.getSelectedFile().getAbsolutePath());
                    FileInputStream fis = null;
                    try {
                        fis = new FileInputStream(chooser.getSelectedFile());
                        byte[] data = new byte[(int) chooser.getSelectedFile().length()];
                        fis.read(data);
                        fis.close();
                        String str = new String(data, "UTF-8");
                        previewRaw.setText(str);
                    } catch (FileNotFoundException e1) {
                        e1.printStackTrace();
                    } catch (UnsupportedEncodingException e1) {
                        e1.printStackTrace();
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }


                }
            }
        });

        selectEncryptedXMLFile.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int ret = chooser.showOpenDialog(frame);
                if (ret == JFileChooser.APPROVE_OPTION) {
                    encryptedXMLFile.setText(chooser.getSelectedFile().getAbsolutePath());
                    FileInputStream fis = null;
                    try {
                        fis = new FileInputStream(chooser.getSelectedFile());
                        byte[] data = new byte[(int) chooser.getSelectedFile().length()];
                        fis.read(data);
                        fis.close();
                        String str = new String(data, "UTF-8");
                        previewEncrypt.setText(str);
                    } catch (FileNotFoundException e1) {
                        e1.printStackTrace();
                    } catch (UnsupportedEncodingException e1) {
                        e1.printStackTrace();
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }
                }
            }
        });


        encrypt.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //System.out.println("hello");
                encryptedDoc = app.encrypt(rawXMLFile.getText(), publickey.getText(), xpathExpress.getText());
                previewRaw.setText(App.nodeToString(encryptedDoc.getDocumentElement()));
            }
        });

        decrypt.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //System.out.println("hello");
                rawDoc = app.decrypt(encryptedXMLFile.getText(), privatekey.getText());
                previewRaw.setText(App.nodeToString(rawDoc.getDocumentElement()));
            }
        });
    }
}
