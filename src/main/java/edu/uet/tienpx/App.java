package edu.uet.tienpx;


import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.crypto.*;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;
import javax.xml.xpath.XPath;

import java.awt.FlowLayout;
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.StringWriter;
import java.security.*;

/**
 * Hello world!
 *
 */
public class App 
{
    private JFrame frame;
    private JPanel panel;
    private JLabel label;
    private JButton button;
    private JFileChooser keybrowser;
    private JFileChooser xmlbrowser;


    public App(){
        createGUI();
    }

    public void createGUI(){
        new GUI(this);
    }

    public static void print(byte[] a){
        for(int i = 0;i < a.length; i++){
            System.out.print(a[i]);
            System.out.print("|");
        }
        System.out.println();
    }

    public void testRSA(){
        try {
            KeyPair keys = KeyPairGenerator.getInstance("RSA").generateKeyPair();
            print(keys.getPublic().getEncoded());
            print(keys.getPrivate().getEncoded());
            Cipher cipher = Cipher.getInstance("RSA");
            cipher.init(Cipher.ENCRYPT_MODE, keys.getPublic());
            byte[] encrypted = cipher.doFinal("hello".getBytes());
            print(encrypted);
            cipher.init(Cipher.DECRYPT_MODE, keys.getPrivate());
            System.out.println((new String(cipher.doFinal(encrypted))));
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        }
    }

    public static String nodeToString(Node node) {
        StringWriter sw = new StringWriter();
        try {
            Transformer t = TransformerFactory.newInstance().newTransformer();
            t.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
            t.transform(new DOMSource(node), new StreamResult(sw));
        } catch (TransformerException te) {
            System.out.println("nodeToString Transformer Exception");
        }
        return sw.toString();
    }


    public Document encrypt(String xmlPath, String keyPath, String xpathExpress){
        try {
            //Document doc = new XDocument(ClassLoader.getSystemResourceAsStream("/xmlsample.xml")).getDocument();

            Document doc = new XDocument(new FileInputStream(xmlPath)).getDocument();
            doc.getDocumentElement().normalize();
            XPath xpath = XPathFactory.newInstance().newXPath();
            NodeList nodeList = (NodeList) xpath.compile(xpathExpress).evaluate(doc, XPathConstants.NODESET);
            System.out.println(nodeList.getLength());

            Node encryped = Crypter.encryptXMLAsym(nodeList.item(0),
                    "JKS",
                    "keys/keystore.jks",
                    "passw0rd",
                    "AES",
                    "DESede",
                    "tienpx.uet.vnu");
            System.out.println(nodeToString(encryped));
            nodeList.item(0).getParentNode().replaceChild(doc.importNode(encryped, true), nodeList.item(0));
            return doc;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public Document decrypt(String xmlPath, String keyPath){
        try {
            //Document doc = new XDocument(ClassLoader.getSystemResourceAsStream("/xmlsample.xml")).getDocument();

            Document doc = new XDocument(new FileInputStream(xmlPath)).getDocument();
            doc.getDocumentElement().normalize();
            XPath xpath = XPathFactory.newInstance().newXPath();
            NodeList nodeList = (NodeList) xpath.compile("//EncryptionWrapper").evaluate(doc, XPathConstants.NODESET);
            Node encryped = nodeList.item(0);
            System.out.println(new XDocument(encryped).select("//EncryptionWrapper/KeyEncryptionKey").item(0).getTextContent());
            Node decrypted = Crypter.decryptXMLAsym(
                    new XDocument(encryped).select("//EncryptionWrapper/*[name()='xenc:EncryptedData']").item(0),
                    new XDocument(encryped).select("//EncryptionWrapper/KeyEncryptionKey").item(0).getTextContent(),
                    "DESede",
                    "JKS",
                    keyPath,
                    "passw0rd",
                    "tienpx.uet.vnu",
                    "passw0rd");
            System.out.println(nodeToString(decrypted));
            System.out.println(nodeToString(encryped));
            encryped.getParentNode().replaceChild(doc.importNode(decrypted, true), encryped);
            return doc;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void main( String[] args )
    {
        App app = new App();
        app.decrypt("D:\\Workspace\\XMLCrypter\\src\\main\\resources\\XML\\encryptedxml.xml", "keys/keystore.jks");
//        System.out.println(nodeToString(app.encrypt("D:\\Workspace\\XMLCrypter\\src\\main\\resources\\XML\\xmlsample.xml","keys/keystore.jks","//CardId" ).getDocumentElement()));
//        try {
//            //Document doc = new XDocument(ClassLoader.getSystemResourceAsStream("/xmlsample.xml")).getDocument();
//
//            Document doc = new XDocument(new FileInputStream("D:\\Workspace\\XMLCrypter\\src\\main\\resources\\XML\\xmlsample.xml")).getDocument();
//            doc.getDocumentElement().normalize();
//            XPath xpath = XPathFactory.newInstance().newXPath();
//            String express = "//CardId";
//            NodeList nodeList = (NodeList) xpath.compile(express).evaluate(doc, XPathConstants.NODESET);
//            System.out.println(nodeList.getLength());
//
//            Node encryped = Crypter.encryptXMLAsym(nodeList.item(0),
//                    "JKS",
//                    "keys/keystore.jks",
//                    "passw0rd",
//                    "AES",
//                    "DESede",
//                    "tienpx.uet.vnu");
//            System.out.println(nodeToString(encryped));
//            System.out.println(new XDocument(encryped).select("/EncryptionWrapper/*[name()='xenc:EncryptedData']").getLength());
//            System.out.println(new XDocument(encryped).select("/EncryptionWrapper/KeyEncryptionKey").item(0).getTextContent());
//            Node decrypted = Crypter.decryptXMLAsym(
//                    new XDocument(encryped).select("/EncryptionWrapper/*[name()='xenc:EncryptedData']").item(0),
//                    new XDocument(encryped).select("/EncryptionWrapper/KeyEncryptionKey").item(0).getTextContent(),
//                    "DESede",
//                    "JKS",
//                    "keys/keystore.jks",
//                    "passw0rd",
//                    "tienpx.uet.vnu",
//                    "passw0rd");
//            System.out.println(nodeToString(decrypted));
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
        //System.out.println( "Hello World!" );
    }
}
